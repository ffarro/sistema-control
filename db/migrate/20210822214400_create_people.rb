class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :nombres
      t.string :apellidoPat
      t.string :apellidoMat
      t.string :dni

      t.timestamps
    end
  end
end
