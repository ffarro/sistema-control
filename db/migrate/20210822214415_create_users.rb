class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password_digest
      t.boolean :estado
      t.references :rol, foreign_key: true
      t.references :person, foreign_key: true

      t.timestamps
    end
    add_index :users, :username, unique: true
  end
end
