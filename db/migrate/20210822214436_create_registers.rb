class CreateRegisters < ActiveRecord::Migration[5.2]
  def change
    create_table :registers do |t|
      t.date :fecha
      t.time :hora
      t.string :tipo
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
