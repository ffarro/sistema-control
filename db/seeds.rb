# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rol.destroy_all

roles = Rol.create([{ 
    descripcion: 'administrador' 
}, 
{ 
    descripcion: 'trabajador' 
}
])

Person.destroy_all
#Person nombres:string apellidoPat:string apellidoMat:string dni:string 

people = Person.create([{ 
    nombres: 'Gabriel',
    apellidoPat: 'García',
    apellidoMat: 'Marquez',
    dni: '12121212'

}, 
{ 
    nombres: 'Lu',
    apellidoPat: 'Godoy',
    apellidoMat: 'Alcayaga',
    dni: '45454545'
},
{ 
    nombres: 'Julio',
    apellidoPat: 'Florencio',
    apellidoMat: 'Cortázar',
    dni: '78787878'
}
])


#Register fecha:date hora:time tipo:string user:references

#Correr los sedders: rails db:seed