class RegistersController < ApplicationController
  load_and_authorize_resource
  #before_action :set_register, only: %i[ show edit update destroy ]
  
  def reporteCH
    @anios = (2018..2021)
    @id = params[:id]
  end
  
  def prueba
    puts "METODO DE PRUEBA DESDE EL CONTROLADOR"
    
    #Parámetros elegidos
    @user_id = params[:user_id]
    @month = Integer(params[:mes_id])
    @year = Integer(params[:anio_id])
    
    #Consulta: todos los registros que están dentro de ese rango
    @range = Date.new(@year, @month)
    @registers = Register.joins(:user).where(  users: {id:@user_id} , registers:{fecha: @range.all_month})
    
    @totalMes = 0
    @lista = {}

    if @registers.length > 0
      puts "SE ENCONTRARON REGISTROS"
      
      (1..31).each do |day|
        @entrada = 0
        @salida = 0

        @registers.each do |item|
          if item.fecha.day == day
              if item.tipo == 'E'
                @entrada = Time.parse(item.hora.strftime("%I:%M %p"))
              end
              if item.tipo == 'S'
                @salida = Time.parse(item.hora.strftime("%I:%M %p"))
              end
          end
        end
        @totalHoras = @salida.hour - @entrada.hour
        @totalMes += @totalHoras

        @lista.store(:"#{day}", @totalHoras)
        
      end
      @lista.each do |key, value|
        puts "El día #{key} trabajaste #{value} horas"
      end
    else
      puts "NO SE ENCONTRARON REGISTROS"
    end
 
    respond_to do |format|
      format.js {render 'reporteCH'}
    end

  end
  

  def cantidadHoras

    @user_id = params[:user_id]
    @month = Integer(params[:mes_id])
    @year = Integer(params[:anio_id])

    @range = Date.new(@year, @month)
    @registers = Register.joins(:user).where(  users: {id:@user_id} , registers:{fecha: @range.all_month})
    
    @totalMes = 0

    if @registers.length > 0
      puts "SE ENCONTRARON REGISTROS"
      
      (1..31).each do |day|
        @entrada = 0
        @salida = 0

        @registers.each do |item|
          if item.fecha.day == day
              if item.tipo == 'E'
                @entrada = Time.parse(item.hora.strftime("%I:%M %p"))
              end
              if item.tipo == 'S'
                @salida = Time.parse(item.hora.strftime("%I:%M %p"))
              end
              puts "AQUI ESTOY #{item.hora} - #{item.tipo}"
          end
        end

        @totalHoras = @salida.hour - @entrada.hour
        @totalMes += @totalHoras
        puts "***************************************"
        puts " El día #{day} trabajaste  #{@totalHoras} horas"
        puts "***************************************"
        
      end

    else
      puts "NO SE ENCONTRARON REGISTROS"
    end
#
#
    #(1..31).each do |day|
#
    #  @entrada = 0
    #  @salida = 0
#
    #  @registers.each do |item|
    #    if item.fecha.day == day
    #        if item.tipo == 'E'
    #          @entrada = Time.parse(item.hora.strftime("%I:%M %p"))
    #        end
    #        if item.tipo == 'S'
    #          @salida = Time.parse(item.hora.strftime("%I:%M %p"))
    #        end
#
    #        puts "AQUI ESTOY #{item.hora} - #{item.tipo}"
    #    end
#
    #  end
    #  
    #  @totalHoras = @salida.hour - @entrada.hour
    #  @totalMes += @totalHoras
    #  puts "***************************************"
    #  puts " El día #{day} trabajaste  #{@totalHoras} horas"
    #  puts "***************************************"
    #end
    
#
    #@registers.each do |item|
    #  puts item.hora
#
    #  if item.tipo == 'E'
    #      @entrada = Time.parse(item.hora.strftime("%I:%M %p"))
    #  end
    #  if item.tipo == 'S'
    #      @salida = Time.parse(item.hora.strftime("%I:%M %p"))
    #  end
    #  
    #end
#
    #@tiempoTotal = (@salida.hour*60 + @salida.min) - (@entrada.hour*60 + @entrada.min)
    #@totalHoras = @salida.hour - @entrada.hour
    #@totalMin =  @salida.min+@entrada.min
#
    #puts "***************************************"
    #puts " Trabajaste #{@totalHoras}:#{@totalMin < 10 ? "0#{@totalMin}":@totalMin} horas"
    #puts "***************************************"
#
  end



  # GET /registers or /registers.json
  def index
    @registers = Register.all
  end

  # GET /registers/1 or /registers/1.json
  def show
  end

  # GET /registers/new
  def new
    @register = Register.new
    @session = session[:user_id]
    @register.id = @session
  end

  # GET /registers/1/edit
  def edit
  end

  # POST /registers or /registers.json
  def create
    @register = Register.new(register_params)

    respond_to do |format|
      if @register.save
        format.html { redirect_to @register, notice: "Register was successfully created." }
        format.json { render :show, status: :created, location: @register }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @register.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registers/1 or /registers/1.json
  def update
    respond_to do |format|
      if @register.update(register_params)
        format.html { redirect_to @register, notice: "Register was successfully updated." }
        format.json { render :show, status: :ok, location: @register }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @register.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registers/1 or /registers/1.json
  def destroy
    @register.destroy
    respond_to do |format|
      format.html { redirect_to registers_url, notice: "Register was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  #Nuevos métodos

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_register
      @register = Register.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def register_params
      params.require(:register).permit(:fecha, :hora, :tipo, :user_id)
    end
end
