class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_username(params[:username])

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id

      if user.estado 
        redirect_to menu_path, notice:"Bienvenido #{user.rol.descripcion}"
      else
        redirect_to login_path, alert: "No tienes los permisos necesarios."
      end
    else 
      session[:user_id] = nil

      redirect_to login_path, notice:'Usuario o contraseña incorrecta'
    end

  end

  def destroy
    session[:user_id] = nil

    redirect_to root_path
    
  end
end
