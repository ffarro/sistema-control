class UsersController < ApplicationController
  load_and_authorize_resource

  
  #before_action :set_user, only: %i[ show edit update destroy ]

  # GET /users or /users.json
  def index
    @users = User.all
  end
  
  # GET /users/1 or /users/1.json
  def show
  end
  
  # GET /users/new
  def new
    @user = User.new
    @roles = Rol.all
    @people = Person.all

  
  end

  # GET /users/1/edit
  def edit
    @roles = Rol.all
    @people = Person.all
  end

  # POST /users or /users.json
  def create
    @user = User.new(user_params)
    @roles = Rol.all
    @people = Person.all

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: "User was successfully created." }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: "User was successfully updated." }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
    @roles = Rol.all
    @people = Person.all
  end

  def habilitar
    @user = User.find(params[:id])
    @user.update_attribute(:estado, true)
    redirect_to users_path
  end
  
  def deshabilitar
    @user = User.find(params[:id])
    @user.update_attribute(:estado, false)
    redirect_to users_path
  end



  # DELETE /users/1 or /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: "User was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :estado, :rol_id, :person_id)
    end
end
