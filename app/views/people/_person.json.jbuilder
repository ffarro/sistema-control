json.extract! person, :id, :nombres, :apellidoPat, :apellidoMat, :dni, :created_at, :updated_at
json.url person_url(person, format: :json)
