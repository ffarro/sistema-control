json.extract! user, :id, :username, :estado, :rol_id, :person_id, :created_at, :updated_at
json.url user_url(user, format: :json)
