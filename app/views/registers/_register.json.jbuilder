json.extract! register, :id, :fecha, :hora, :tipo, :user_id, :created_at, :updated_at
json.url register_url(register, format: :json)
