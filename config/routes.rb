Rails.application.routes.draw do
  get 'calendario/index'
  get 'menu', to: 'menu#index' #Mostrar el menú
  get 'login', to: 'sessions#new' #Mostrar el formulario
  post 'login', to: 'sessions#create' #Crear la sesión
  get 'logout', to: 'sessions#destroy'
  
  resources :registers
  resources :users
  resources :people
  resources :rols

  post 'users/habilitar'
  post 'users/deshabilitar'

  get 'reporte1', to: 'registers#reporteCH'
  post 'reporte1', to: 'registers#reporteCH'
  post 'registers/cantidadHoras'
  post 'registers/prueba'
  
  get 'calendario', to: 'calendario#index'

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
